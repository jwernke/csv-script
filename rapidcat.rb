#! /usr/bin/env ruby

require 'fileutils'
require 'pp'
require 'json'
require 'net/http'
require 'uri'

Dir.glob('./shopify/*') do |item|

    File.open(item).each do |line|
        #split items in line
        attributes = []

        items = line.split(',')
        varientId = items[1]

        # remove a few items from the array
        # define new metafields array
        metafields = items
        metafields.shift();
        metafields.shift();
        metafields.shift();
   

        # Define Count and length
        count = 0 
        numMeta = metafields.length
  

        while count < numMeta
            # puts count
            # puts metafields[count]
            # puts metafields[count+1]

            if (metafields[count] != '' && metafields[count] != nil && metafields[count +1 ] != '' && metafields[count+1] != nil)
                puts metafields[count] + metafields[count+1]
                attributes.push({
                    "key": metafields[count],
                    "value": metafields[count + 1],
                    "namespace": "snow-axis",
                    "value_type": "string"
                })
            end
            count += 2
            # while loop ends here 
        end

        # assign items to object
        object = {
            'variant_id' => varientId, 
            'config': [
                {
                    'host' => "viacom-sbsp.myshopify.com",
                    "apiKey": "23c3625622279689c5aac08b13fbc0d5"
                }
            ],
            'custom_attributes' => attributes
        }
       
        # Object to send to Rest API
         #pp object.to_json

        uri = URI.parse("http://localhost:4000/api/shopify/metafield/variant/")

        header = {'Content-Type': 'application/json', 'X-Snowcommerce-Retail-Cataloger': 'asdfasdf' }
        
         # Create the HTTP objects
        http = Net::HTTP.new(uri.host, uri.port)
        request = Net::HTTP::Post.new(uri.request_uri, header)
        request.body = object.to_json

        # # # call API
        response = http.request(request)

    end

end
